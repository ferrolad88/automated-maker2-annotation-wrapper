# AMAW - Automated MAKER2 Annotation Wrapper

## Installation

Please first install Singularity v3: <https://sylabs.io/guides/3.0/user-guide/installation.html>

### Clone the repository and download auxiliary data

    $ git clone https://bitbucket.org/phylogeno/amaw/src/master/
    $ cd master
    $ wget https://figshare.com/ndownloader/files/31658918 -O taxdump.tgz
    $ tar -xzf taxdump.tgz
    $ wget https://figshare.com/ndownloader/files/31658921 -O prot_db.tgz
    $ tar -xzf prot_db.tgz

### Install dependencies

Download the following packages and place them in the current AMAW folder.
Make sure to use the exact versions listed below.

#### Trinity

Visit: <https://github.com/trinityrnaseq/trinityrnaseq/wiki>

    $ wget https://github.com/trinityrnaseq/trinityrnaseq/archive/refs/tags/Trinity-v2.4.0.tar.gz  

#### Bowtie 2

Visit: <https://github.com/BenLangmead/bowtie2>

    $ wget https://github.com/BenLangmead/bowtie2/releases/download/v2.3.5/bowtie2-2.3.5-linux-x86_64.zip

#### Jellyfish

Visit: <https://github.com/gmarcais/Jellyfish>

    $ wget https://github.com/gmarcais/Jellyfish/releases/download/v2.3.0/jellyfish-2.3.0.tar.gz

#### Salmon

Visit: <https://github.com/COMBINE-lab/salmon>

    $ wget https://github.com/COMBINE-lab/salmon/releases/download/v1.5.2/salmon-1.5.2_linux_x86_64.tar.gz

#### NCBI-BLAST+

Visit: <https://ftp.ncbi.nlm.nih.gov/blast/executables/blast+>

    $ wget https://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.10.0/ncbi-blast-2.10.0+-x64-linux.tar.gz

#### Tandem Repeats Finder

Visit: <https://tandem.bu.edu/trf/trf409.linux64.download.html>

Download the `trf409.linux64` version, without the limitation on `glibc`.

#### RMBlast

Visit: <http://www.repeatmasker.org/RMBlast.html>

    $ wget http://www.repeatmasker.org/rmblast-2.9.0+-p2-x64-linux.tar.gz

#### RepeatMasker

Visit: <https://www.repeatmasker.org/RepeatMasker/>

    $ wget https://www.repeatmasker.org/RepeatMasker/RepeatMasker-4.1.1.tar.gz

#### Exonerate

Visit: <https://www.ebi.ac.uk/about/vertebrate-genomics/software/exonerate>

    $ wget http://ftp.ebi.ac.uk/pub/software/vertebrategenomics/exonerate/exonerate-2.2.0-x86_64.tar.gz  

#### MAKER2

Visit: <https://www.yandell-lab.org/software/maker.html>

Register and download MAKER2.

### Build the container

Make sure to have root privileges on your system before building the container.  
Make sure to have all dependencies in your current directory before building the container; these files or directories should be present:

    Trinity-v2.4.0.tar.gz 
    bowtie2-2.3.5-linux-x86_64.zip 
    jellyfish-2.3.0.tar.gz 
    salmon-1.5.2_linux_x86_64.tar.gz 
    ncbi-blast-2.10.0+-x64-linux.tar.gz 
    trf409.linux64 
    rmblast-2.9.0+-p2-x64-linux.tar.gz 
    RepeatMasker-4.1.1.tar.gz 
    exonerate-2.2.0-x86_64.tar.gz 
    maker-2.31.11.tgz 
    amaw.pl 
    transcript-filter.pl 
    prot_db/
    convert_fathom2genbank.pl 
    align_and_estimate_abundance.pl

    $ sudo singularity build amaw.sif amaw.def

## Usage

Execute the container.
Make sure to bind directories (see <https://sylabs.io/guides/3.0/user-guide/bind_paths_and_mounts.html> for examples).
In the following lines, `<PATH-to-DIRECTORY>` corresponds to your local directory.
Additional files and directories (e.g., `taxdump`) are not meant to be included in the container but will be required when running AMAW.

Run AMAW:

    $ wget https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/001/653/735/GCA_001653735.1_Ucit_macronuclear_v.1.0/GCA_001653735.1_Ucit_macronuclear_v.1.0_genomic.fna.gz  
    $ singularity exec --bind <PATH-to-DIRECTORY>:/mnt amaw.sif amaw.pl 
        --genome=/mnt/GCA_001653735.1_Ucit_macronuclear_v.1.0_genomic.fna \
        --organism=Uroleptopsis_citrina --proteins=1 --est=1 --taxdir=/mnt/taxdump/ \
        --maker-cpus=50 --trinity-cpus=50 --rsem-cpus=50 \
        --augustus-db=/mnt/augustus-config/ --augustus-gm=tetrahymena \
        --outdir=/mnt/GCA_001653735.1 --prot-dbs=/mnt/prot_db/

`taxdump`, `augustus-config` and `prot_dbs` are the corresponding directories from this repository.
`augustus-config` can be updated from augustus GitHub: <https://github.com/Gaius-Augustus/Augustus>

## Copyright and License

This software is copyright (c) 2017-2021 by University of Liege / Sciensano / BCCM / Loic MEUNIER, Denis BAURAIN and Luc CORNET.
This is free software; you can redistribute it and/or modify.
